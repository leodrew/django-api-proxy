# django-api-proxy

NGINX proxy app for django-api

### Enviroment Variables

 * `LISTEN_PROT` - Port to listen on (default: `8000`)
 * `APP_HOST` - Hostname of the app to forward requests to  (default `app`)
 * `APP_PORT` - Port of the app to forward requests to (default `9000`)